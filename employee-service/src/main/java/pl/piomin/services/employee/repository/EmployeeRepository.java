package pl.piomin.services.employee.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.piomin.services.employee.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long>{
	public List<Employee> findByDepartmentId(Long departmentId);
	public List<Employee> findByOrganizationId(Long organizationId);
}
